import { assert } from 'chai';
import 'mock-local-storage';
const sleep = Promise.resolve();

if (typeof global.sessionStorage === 'undefined') {
    global.sessionStorage || global.localStorage;
}

import { undying } from '../src/index.js';

describe('Undying', function () {
    afterEach(() => {
        localStorage.removeItem('hello');
        localStorage.removeItem('array');
        localStorage.removeItem('restore');
    });
    describe('sessionStorage', () => {
        it('Should save on session', (done) => {
            const x = undying.session('session', { test: 2 });
            undying.observe(x, (value) => {
                assert.deepEqual(x, JSON.parse(sessionStorage.getItem('session')));
                assert.strictEqual(value.status, 'Pass');
                done();
            });
            x.status = 'Pass';
        });
    });
    describe('Observing', () => {
        it('Should observe on existing', (done) => {
            const x = undying('hello', { test: 1 });
            undying.observe(x, (value) => {
                assert.deepEqual(x, value);
                assert.strictEqual(value.name, 'Avichay');
                done();
            });
            x.name = 'Avichay';
        });

        it('Should throw for non undying observe', (done) => {
            const x = {};
            try {
                undying.observe(x, () => {
                    assert.fail('This should not happen');
                });
            } catch (err) {
                assert.ok(err);
                done();
            }
            x.something = 42;
        });
    });
    describe('Working with arrays', () => {
        it('Should observe array changes', async () => {
            const x = undying('array', {
                arr: []
            });
            x.arr.push(1, 2, 3);
            await sleep;
            assert.equal(localStorage.getItem('array'), JSON.stringify(x));
        });

        it('Should proxify nested objects', async () => {
            const x = undying('array', {
                arr: [1, 2, 3]
            });
            x.arr.push({ name: 'Avichay'} );
            await sleep;
            assert.equal(localStorage.getItem('array'), JSON.stringify(x));
        });
    });
    describe('New data', function () {
        it('Should create an object', async () => {
            const x = undying('hello');
            x.y = 8;
            x.r = {
                hello: 'world'
            };
            await sleep;
            assert.equal(localStorage.getItem('hello'), JSON.stringify(x));
        });

        it('Should create a default value object', async () => {
            const stub = { myAge: 42 };
            const x = undying('hello', stub);
            await sleep;
            assert.equal(localStorage.getItem('hello'), JSON.stringify(stub));
            assert.equal(localStorage.getItem('hello'), JSON.stringify(x));
        });
    });

    describe('Existing data', function () {
        beforeEach(async () => {
            const x = undying('restore');
            x.data = {
                user: 'Avichay',
                age: 42,
            };
            x.num = 42;
            await sleep;
        });

        it('Should have data', () => {
            const x = undying('restore');
            assert.equal(x.data.user, 'Avichay');
            assert.equal(x.data.age, 42);
            assert.equal(x.num, 42);
        });

        it('Should ignore default when previously saved', () => {
            const x = undying('restore', { pach: 'control' });
            assert.equal(x.data.user, 'Avichay');
            assert.equal(x.data.age, 42);
            assert.equal(x.num, 42);
            assert.isUndefined(x.pach);
        })
    });
})