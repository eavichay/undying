# _undying_ 🧟🧛🧟👻🧛🧟
## Tiny & fast "auto-save" data objects to local storage or session storage
[![Pretty small, eh?](https://badgen.net/bundlephobia/minzip/undying)](https://bundlephobia.com/result?p=undying)

### Usage

```javascript

import { undying } from 'undying';


const defaultValues = {
    favouriteColor: 'blue',
};

const undyingObject = undying('storage-key', defaultValues);
// If exists on local storage, ignores default values,
// If does not exists, creates it with default values


undyingObject.favouriteColor = 'red';
// async flushed to local storage.

```

An undying object is a JSON-format object proxy, flushing changes into localStorage.
When an undying object's property is assigned with another object (or array), the actual saved value is an undying object. This means mutations in the object's inner tree and even array mutations are caught and saved into the localStorage.

Every change triggers an async flush - meaning that multiple changes wait until the end of the current execution stack and the store occurs only once in a dedicated microtask.

### Session storage
```javascript
import { undying } from 'undying';

const undyingData = undying.session('storage-key', {});
```


### Watching changes

```javascript
import { undying } from 'undying';
const undyingObject = undying('user-defaults'); // defaults to an empty object, otherwise last stored data

undying.observe(undyingObject, (value) => {
    // value is the whole tree data
});

```