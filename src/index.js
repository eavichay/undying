import { proxify, proxyHandlers, watchers } from './createProxy.js';

export function undying(namespace, defaultValue = {}, storage = localStorage) {
    let temp;
    if (!(temp = storage.getItem(namespace))) {
        storage.setItem(namespace, JSON.stringify(defaultValue));
        temp = defaultValue;
    } else {
        temp = JSON.parse(temp);
    }
    const cb = (o) => {
        storage.setItem(namespace, JSON.stringify(o))
    };
    return proxify(temp, cb);
}

undying.observe = function (proxified, cb) {
    if (!proxyHandlers.get(proxified)) {
        throw new Error('Target is not undying object');
    }
    watchers.get(proxyHandlers.get(proxified).root).push(cb);
}

undying.session = function(namespace, defaultValue = {}) {
    return undying(namespace, defaultValue, sessionStorage);
}
