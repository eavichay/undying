export const handlers = new WeakMap();
export const proxyHandlers = new WeakMap();
export const watchers = new WeakMap();

/** @returns ProxyHandler */
const createHandler = (cb, rootElement) => {
    /** @type ProxyHandler */
    let handler = handlers.get(rootElement);
    if (!handler) {
        handler = {
            get (target, key) {
                return target[key];
            },
            set (target, key, value) {
                target[key] = createProxy(value, cb, rootElement);
                this.flush();
                return true;
            },
            deleteProperty (target, key) {
                delete target[key];
                this.flush();
                return true;
            },
            defineProperty (target, p, descriptor) {
                Object.defineProperty(target, p, descriptor);
                this.flush();
                return true;
            },
            flushing: false,
            flush () {
                if (this.flushing) {
                    return;
                } else {
                    this.flushing = true;
                    Promise.resolve().then( () => {
                        watchers.get(rootElement).forEach(fn => fn(rootElement));
                        this.flushing = false;
                    });
                }
            },
            root: rootElement,
        };
        handlers.set(rootElement, handler);
    };
    if (!watchers.get(rootElement)) {
        watchers.set(rootElement, [() => cb(rootElement)]);
    };
    return handler;
};

export function createProxy (target, cb, root) {
    if (typeof target === 'object' && target !== null && !Array.isArray(target)) {
        const handler = createHandler(cb, root || target)
        const p = new Proxy(target, handler);
        proxyHandlers.set(p, handler);
        Object.keys(target).forEach(key => {
            p[key] = target[key];
        });
        return p;
    };
    if (Array.isArray(target)) {
        return proxifyArray(target, cb, root || target);
    };
    return target;
};

function proxifyArray(arr, cb, root) {
    const newProto = {
        __proto__: Array.prototype,
    };

    ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'].forEach(fnName => {
        newProto[fnName] = function () {
            const newArgs = Array.from(arguments).map(arg => createProxy(arg, cb, root));
            Array.prototype[fnName].apply(arr, newArgs);
            cb(root);
        };
    });

    arr.__proto__ = newProto;
    return arr;
}

export function proxify(target, cb) {
    return createProxy(target, cb, target);
}